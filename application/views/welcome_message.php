<!DOCTYPE html>
<html>
<head>
  <title>LANDAZURI</title>
  <meta charset="utf-8" />
  <meta
  name="viewport"
  content="width=device-width, initial-scale=1, user-scalable=no"
  />
  <!-- icon -->
  <link rel="icon" href="<?= base_url()?>images/avatar.png" type="image/png" />
  <!-- style -->
  <link rel="stylesheet" href="<?= base_url()?>assets/css/main.css" />

  
  <!-- Bootstrap CSS -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

  <style>
    .patron {
      max-width: 100%;
      height: auto;
      display: block; /* Para evitar espacios adicionales debajo de la imagen */
      margin: auto; /* Centrar la imagen dentro de su contenedor */
    }
  </style>
</head>
<body class="is-preload">
  <!-- header -->
  <header id="header">
    <div class="inner">
      <a href="#" class="image avatar"
      ><img src="images/avatar.png" alt=""
      /></a>
      <h1>
        <strong>GUILLERMO LANDAZURI AMAYA </strong><br />
        Arquitectura de software<br />
        <a href="<?= base_url()?>" rel="noopener noreferrer">Actividad 1 - Glosario de conceptos</a><br />
        <a href="<?= base_url()?>actividad3" rel="noopener noreferrer">Actividad 3 - Arquitectura Cliente Servidor</a><br />
        <a href="<?= base_url()?>actividad4" rel="noopener noreferrer">Actividad 4 - Desarrollo de una arquitectura basada-cliente</a> <br />
        <a href="<?= base_url()?>actividad6" rel="noopener noreferrer">Actividad 6 : Aplicativo de arquitectura de software</a>
      </h1>
    </div>
  </header>
  <!-- main -->
  <div id="main">
    <section>
      <header class="major">
        <h2>Arquitectura Monolítica</h2>
      </header>
      <p>
        La arquitectura monolítica es un enfoque de diseño de software donde
        todos los componentes de una aplicación están interconectados y se
        ejecutan en un solo proceso. Es fácil de desarrollar y mantener, pero
        puede ser menos escalable.
      </p>

      <blockquote>
        Ejemplo: WordPress es un ejemplo común de arquitectura monolítica,
        donde el núcleo de la aplicación gestiona tanto el frontend como el
        backend de un sitio web.
      </blockquote>

      <div class="box alt">
        <div class="row gtr-50 gtr-uniform">
          <div class="col-12">
            <img class="patron" src="images/wordpress.png" />
          </div>
        </div>
      </div>
    </section>

    <section>
      <header class="major">
        <h2>Modelo Vista Controlador (MVC)</h2>
      </header>
      <p>
        MVC es un patrón arquitectónico que separa una aplicación en tres
        componentes principales: Modelo (datos y lógica), Vista (presentación)
        y Controlador (gestión de la interacción). Esto mejora la modularidad
        y la mantenibilidad del código.
      </p>

      <blockquote>
        Ejemplo: En el desarrollo web, frameworks como Django y Ruby on Rails
        siguen el patrón MVC, donde el Modelo maneja la base de datos, la
        Vista presenta la información y el Controlador gestiona las
        solicitudes del usuario.
      </blockquote>

      <div class="box alt">
        <div class="row gtr-50 gtr-uniform">
          <div class="col-12">
            <img class="patron" src="images/mvc.png" />
          </div>
        </div>
      </div>
    </section>

    <section>
      <header class="major">
        <h2>Arquitectura Orientada a Servicios (SOA)</h2>
      </header>
      <p>
        SOA es un enfoque que organiza una aplicación como un conjunto de
        servicios independientes, comunicándose a través de interfaces bien
        definidas. Facilita la interoperabilidad, la reutilización y la
        evolución independiente de los servicios.
      </p>

      <blockquote>
        Ejemplo: Amazon utiliza la arquitectura orientada a servicios en su
        infraestructura, donde servicios como S3 para almacenamiento y EC2
        para cómputo son componentes independientes que colaboran para ofrecer
        funcionalidades completas.
      </blockquote>

      <div class="box alt">
        <div class="row gtr-50 gtr-uniform">
          <div class="col-12">
            <img class="patron" src="images/amazon.png" />
          </div>
        </div>
      </div>
    </section>

    <section>
      <header class="major">
        <h2>Patrón de Inyección de Dependencias (DI)</h2>
      </header>
      <p>
        El patrón DI permite la inversión del control al suministrar
        dependencias externas a un componente en lugar de que este las cree
        internamente. Mejora la modularidad, la prueba y la flexibilidad al
        desacoplar componentes.
      </p>

      <blockquote>
        Ejemplo: En Angular, la inyección de dependencias se utiliza para
        proporcionar servicios a componentes, permitiendo la reutilización de
        lógica de negocio y facilitando la prueba de unidades.
      </blockquote>

      <div class="box alt">
        <div class="row gtr-50 gtr-uniform">
          <div class="col-12">
            <img class="patron" src="images/angular.png" />
          </div>
        </div>
      </div>
    </section>

    <section>
      <header class="major">
        <h2>Patrón de Código Limpio (Clean Architecture)</h2>
      </header>
      <p>
        La arquitectura limpia propone organizar el código en capas
        concéntricas, donde las capas internas contienen lógica de negocios y
        las externas representan detalles de implementación. Permite la
        independencia de frameworks y mejora la mantenibilidad y la prueba.
      </p>

      <blockquote>
        Ejemplo: En proyectos Java, Clean Architecture se implementa para
        separar las capas de negocio, infraestructura y presentación,
        permitiendo el cambio de frameworks sin afectar la lógica central.
      </blockquote>

      <div class="box alt">
        <div class="row gtr-50 gtr-uniform">
          <div class="col-12">
            <img class="patron" src="images/java.png" />
          </div>
        </div>
      </div>
    </section>
  </div>
  <!-- Footer -->
  <footer id="footer">
    <div class="inner">
      <ul class="copyright">
        <li>INGENIERIA DE SOFTWARE - IBERO 2024</li>
      </ul>
    </div>
  </footer>

  <!-- Option 1: Bootstrap Bundle with Popper -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>



  <!-- Scripts -->
  <script src="<?= base_url()?>assets/js/jquery.min.js"></script>
  <script src="<?= base_url()?>assets/js/jquery.poptrox.min.js"></script>
  <script src="<?= base_url()?>assets/js/browser.min.js"></script>
  <script src="<?= base_url()?>assets/js/breakpoints.min.js"></script>
  <script src="<?= base_url()?>assets/js/util.js"></script>
  <script src="<?= base_url()?>assets/js/main.js"></script>
</body>
</html>
