<!DOCTYPE html>
<html>
<head>
  <title>LANDAZURI</title>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no"/>
  <!-- icon -->
  <link rel="icon" href="<?= base_url()?>images/avatar.png" type="image/png" />
  <!-- style -->
  <link rel="stylesheet" href="<?= base_url()?>assets/css/main.css" />
  <!-- Bootstrap CSS -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
  <style>
    #botonFlotante {
      position: fixed;
      bottom: 20px; 
      right: 20px; 
      z-index: 9999; 
    }

    .encue-label {
      display: inline-block;
      cursor: pointer;
      font-size: 24px;
      margin: 20px;
    }

    .encue-input {
      display: none; 
    }

    .encue-label span {
      display: inline-block;
      width: 70px;
      height: 70px;
      border: 2px solid transparent; 
      text-align: center;
      line-height: 46px; 
      transition: all 0.3s ease; 
    }
    .encue-input:checked + span {
      border-color: #009688; 
      color: #009688; 
      border-radius: 5px;
      transform: scale(1.2); 
    }
    #avatar-down {
      transform: rotate(180deg); 
    }

    #result {
      margin-top: 20px;
    }

  </style>
</head>
<body class="is-preload">

  <!-- Modal registro -->
  <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl2 modal-dialog-centered">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Agregar eventos</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <form method="post" action="">
          <div class="modal-body">

            <label class="encue-label">
              <input type="radio" name="avatar" id="avatar-up" class="encue-input" value="1" required>
              <span><img src="assets/ev/1.png" width="65px"></span>
            </label>

            <label class="encue-label">
              <input type="radio" name="avatar" id="avatar-up" class="encue-input" value="2">
              <span><img src="assets/ev/2.png" width="65px"></span>
            </label>

            <label class="encue-label">
              <input type="radio" name="avatar" id="avatar-up" class="encue-input" value="3">
              <span><img src="assets/ev/3.png" width="65px"></span>
            </label>

            <label class="encue-label">
              <input type="radio" name="avatar" id="avatar-up" class="encue-input" value="4">
              <span><img src="assets/ev/4.png" width="65px"></span>
            </label>

            <label class="encue-label">
              <input type="radio" name="avatar" id="avatar-up" class="encue-input" value="5">
              <span><img src="assets/ev/5.png" width="65px"></span>
            </label>

            <label class="encue-label">
              <input type="radio" name="avatar" id="avatar-up" class="encue-input" value="6">
              <span><img src="assets/ev/6.png" width="65px"></span>
            </label>

            <label class="encue-label">
              <input type="radio" name="avatar" id="avatar-up" class="encue-input" value="7">
              <span><img src="assets/ev/7.png" width="65px"></span>
            </label>

            <label class="encue-label">
              <input type="radio" name="avatar" id="avatar-up" class="encue-input" value="8">
              <span><img src="assets/ev/8.png" width="65px"></span>
            </label>

            <label class="encue-label">
              <input type="radio" name="avatar" id="avatar-up" class="encue-input" value="9">
              <span><img src="assets/ev/9.png" width="65px"></span>
            </label>

            <label class="encue-label">
              <input type="radio" name="avatar" id="avatar-up" class="encue-input" value="10">
              <span><img src="assets/ev/10.png" width="65px"></span>
            </label>

            <label class="encue-label">
              <input type="radio" name="avatar" id="avatar-up" class="encue-input" value="11">
              <span><img src="assets/ev/11.png" width="65px"></span>
            </label>

            <label class="encue-label">
              <input type="radio" name="avatar" id="avatar-up" class="encue-input" value="12">
              <span><img src="assets/ev/12.png" width="65px"></span>
            </label>

            <div class="mb-3 row">
              <label for="evento" class="col-sm-2 col-form-label">Evento</label>
              <div class="col-sm-10">
                <input type="text" name="evento" class="form-control" id="evento" value="" required>
              </div>
            </div>

            <div class="mb-3 row">
              <label for="fecha" class="col-sm-2 col-form-label">Fecha</label>
              <div class="col-sm-10">
                <input type="date" name="fecha" class="form-control" id="fecha" value="" required>
              </div>
            </div>

            <div class="mb-3 row">
              <label for="nota" class="col-sm-2 col-form-label">Nota</label>
              <div class="col-sm-10">
                <textarea name="nota" id="nota"  rows="6" required></textarea>
              </div>
            </div>

          </div>
          <div class="modal-footer" style="margin-bottom: -30px;" >
            <button type="submit" name="registrar" class="button primary">GUARDAR</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <!-- header -->
  <header id="header">
    <div class="inner">
      <a  class="image avatar"
      ><img src="images/avatar.png" alt=""
      /></a>
      <h1>
        <strong>GUILLERMO LANDAZURI AMAYA </strong><br />
        Arquitectura de software<br />
        <a href="<?= base_url()?>"  rel="noopener noreferrer">Actividad 1 - Glosario de conceptos</a><br />
        <a href="<?= base_url()?>actividad3"  rel="noopener noreferrer">Actividad 3 - Arquitectura Cliente Servidor</a><br />
        <a href="<?= base_url()?>actividad4" rel="noopener noreferrer">Actividad 4 - Desarrollo de una arquitectura basada-cliente</a><br />
        <a href="<?= base_url()?>actividad6" rel="noopener noreferrer">Actividad 6 : Aplicativo de arquitectura de software</a>
      </h1>
    </div>
  </header>
  <!-- main -->
  <div id="main">
    <section>
      <header class="major">
        <div>
          <h2> Agenda de eventos </h2> 
          <h4> Eventos del dia.. </h4> 
        </div>
      </header>
      <button type="button" id="botonFlotante" class="button primary  icon solid fa-plus" data-bs-toggle="modal" data-bs-target="#exampleModal"></button>
      <div class="row" style="margin-top: 25px" >
        <?php 
        $hoy = date("Y-m-d");     

        foreach ($eventos as $key ) {  if ($key->fecha == $hoy ) {

         ?>
         <div class="col-sm-2">
          <div class="card shadow-lg p-3 mb-5 bg-body rounded">
            <div class="card-body">
              <center>
                <img src="assets/ev/<?= $key->avatar ?>.png" alt="" style=" width: 100px; margin-bottom: 10px;"/>
              </center>
              <h5 class="card-title"> <?= $key->evento ?> </h5>
              <p class="card-text"><?= $key->nota ?>  <br> <?= $key->fecha ?> </p>
            </div>
          </div>
        </div>
      <?php } } ?>
    </div>

    <div class="table-wrapper">
      <table>
        <thead>
          <tr>
            <th>Evento</th>
            <th>Fecha</th>
            <th>Nota</th>
            <th>Acciones</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($eventos as $key ) {   ?>
            <tr>
              <td><?= $key->evento ?></td>
              <td><?= $key->fecha ?></td>
              <td><?= $key->nota ?></td>
              <td><button type="submit" class="button primary" data-bs-toggle="modal" data-bs-target="#contacto<?= $key->ideventos ?>">ACTUALIZAR</button></td>
            </tr>

            <!-- Modal actualizar -->
            <div class="modal fade" id="contacto<?= $key->ideventos ?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog modal-xl2 modal-dialog-centered">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">ACTUALIZAR DATOS</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                  </div>
                  <form method="post" action="">
                    <div class="modal-body">

                      <div class="mb-3 row">
                        <label for="staticEmail" class="col-sm-2 col-form-label">Evento</label>
                        <div class="col-sm-10">
                          <input type="text"  class="form-control" name="evento" id="staticEmail" value="<?= $key->evento ?>" required>
                        </div>
                      </div>

                      <div class="mb-3 row">
                        <label for="staticEmail" class="col-sm-2 col-form-label">fecha</label>
                        <div class="col-sm-10">
                          <input type="date"  class="form-control" name="fecha" id="staticEmail" value="<?= $key->fecha ?>" required>
                        </div>
                      </div>

                      <div class="mb-3 row">
                        <label for="staticEmail" class="col-sm-2 col-form-label">Nota</label>
                        <div class="col-sm-10">
                          <textarea name="nota" id="nota"  rows="6" required><?= $key->nota ?></textarea>
                        </div>
                      </div>

                    </div>
                    <div class="modal-footer" style="margin-bottom: -5px;" >
                      <button type="submit" name="eleminar" value="<?= $key->ideventos ?>" class="button ">ELIMINAR</button>
                      <button type="submit" name="actualizar" value="<?= $key->ideventos ?>" class="button primary">ACTUALIZAR</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          <?php }  ?>
        </tbody>
      </table>
    </div>
  </section>


</div>
<!-- Footer -->
<footer id="footer">
  <div class="inner">
    <ul class="copyright">
      <li>INGENIERIA DE SOFTWARE - IBERO 2024</li>
    </ul>
  </div>
</footer>
<!-- Option 1: Bootstrap Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
<!-- Scripts -->
<script src="<?= base_url()?>assets/js/jquery.min.js"></script>
<script src="<?= base_url()?>assets/js/jquery.poptrox.min.js"></script>
<script src="<?= base_url()?>assets/js/browser.min.js"></script>
<script src="<?= base_url()?>assets/js/breakpoints.min.js"></script>
<script src="<?= base_url()?>assets/js/util.js"></script>
<script src="<?= base_url()?>assets/js/main.js"></script>
</body>
</html>