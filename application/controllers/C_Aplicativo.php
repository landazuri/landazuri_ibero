<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_Aplicativo extends CI_Controller {

	public function index(){
		$this->load->model("M_Aplicativo");
		$vista = array();
		if (isset($_POST['registrar'])) {
			$datos = array();
			$datos ['evento']=$_POST['evento'];
			$datos ['fecha']=$_POST['fecha'];
			$datos ['nota']=$_POST['nota'];
			$datos ['avatar']=$_POST['avatar'];
			$this->M_Aplicativo->insertar($datos);
		}

		if (isset($_POST['eleminar'])) {
			$this->M_Aplicativo->borrar($_POST['eleminar']);
		}

		if (isset($_POST['actualizar'])) {
			$datos = array();
			$datos ['evento']=$_POST['evento'];
			$datos ['fecha']=$_POST['fecha'];
			$datos ['nota']=$_POST['nota'];
			$this->M_Aplicativo->actualizar($_POST['actualizar'],$datos);
		}

		$vista ["eventos"] = $this->M_Aplicativo->listar();
		$this->load->view('Aplicativo', $vista);
	}
}