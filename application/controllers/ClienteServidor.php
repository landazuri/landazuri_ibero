<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ClienteServidor extends CI_Controller {

	public function index(){
		$this->load->model("M_Contacto");
		$vista = array();
		if (isset($_POST['registrar'])) {
			$datos = array();
			$datos ['nombre']=$_POST['nombre'];
			$datos ['apellido']=$_POST['apellido'];
			$datos ['telefono']=$_POST['telefono'];
			$datos ['avatar']=$_POST['avatar'];
			$this->M_Contacto->insertar($datos);
		}

		if (isset($_POST['eleminar'])) {
			$this->M_Contacto->borrar($_POST['eleminar']);
		}

		if (isset($_POST['actualizar'])) {
			$datos = array();
			$datos ['nombre']=$_POST['nombre'];
			$datos ['apellido']=$_POST['apellido'];
			$datos ['telefono']=$_POST['telefono'];
			$this->M_Contacto->actualizar($_POST['actualizar'],$datos);
		}

		$vista ["contactos"] = $this->M_Contacto->listar();
		$this->load->view('Tareas', $vista);
	}
}